# MyWishList

Projet PHP
Eros Chiappini
Paul Louppe
Martin Widehem
François Xu

Lien WebEtu : https://webetu.iutnc.univ-lorraine.fr/www/louppe7u/TD/projet/

## Installation

```bash

git clone git@bitbucket.org:paullouppe1/projet.git

```

Les paramètres de la connexion sont dans le fichier db.config.ini dans src/Configuration

| Paramètre     | Valeur           |
| :------------:|:----------------:|
| driver        | mysql            |
| host          | localhost        |
| database      | phptd            |
| username      | root             |
| password      |                  |
| charset       | utf8             |

## Utilisation

importer la base de donnée via le fichier bdd_prohet_php.sql et modifier le fichier db.config.ini dans le 
repertoire src/configuration
## Fonctionnalités

### Participant

- [x] *Afficher une liste de souhaits* (François Xu)
      - L'affichage n'est pas restreint pour le propriétaire à cause d'un problème de la connexion
- [x] *Afficher un item d'une liste* ()
- [x] *Réserver un item* (François Xu)
- [x] *Ajouter un message avec sa réservation* (François Xu)
- [x] *Ajouter un message sur une liste* (François Xu)

### Créateur
- [x] *Créer une liste* (Paul Louppe )
- [x] *Modifier les informations générales d'une de ses listes* (François Xu)
      - La modification n'est pas restreint pour le propriétaire à cause d'un problème de la connexion
- [x] *Ajouter des items* (François Xu)
      - L'ajout n'est pas restreint pour le propriétaire à cause d'un problème de la connexion
- [x] *Modifier un item* (François Xu)
      - La modification n'est pas restreint pour le propriétaire à cause d'un problème de la connexion
- [x] *Supprimer un item* (Paul Louppe)
- [x] *Rajouter une image à un item* (Eros CHiappini/Martin Widehem)
- [x] *Modifier une image à un item* ()
- [x] *Supprimer une image d'un item* (Martin Widehem)
- [x] *Partager une liste* (Martin Widehem)
- [x] *Consulter les réservations d'une de ses listes avant échéance* (Martin Widehem)
- [x] *Consulter les réservations et messages d'une de ses listes après échéance* (François Xu/Martin Widehem)

### Extensions
- [x] *Créer un compte* (François Xu)
- [x] *S'authentifier* (François Xu)
- [x] *Modifier son compte* (François Xu)
- [x] *Rendre une liste publique* (François Xu)
      - L'action n'est pas restreint pour le propriétaire à cause d'un problème de la connexion
- [x] *Afficher les listes de souhaits publiques* (François Xu)
- [ ] *Créer une cagnotte sur un item*
- [ ] *Participer à une cagnotte*
- [x] *Uploader une image* (Paul Louppe, Chiappini Eros)
- [ ] *Créer un compte participant*
- [ ] *Afficher la liste des créateurs*
- [ ] *Supprimer son compte*
- [ ] *Joindre les listes à son compte*