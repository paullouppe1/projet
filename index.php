<?php

use wishlist\Authentification;
use wishlist\Configuration\ConnectionBD;
use Slim\Slim;
use wishlist\controleur\ControleurAcceuil;
use wishlist\controleur\ControleurFomulaire;
use wishlist\controleur\ControleurItem;
use wishlist\controleur\ControleurListe;
use wishlist\controleur\ControleurUtilisateur;
use wishlist\Liste;

require_once __DIR__ . '/vendor/autoload.php';

//echo session_save_path();


try {
    ConnectionBD::connect();
} catch (Exception $e) {}

$app = new Slim();

$app->get('/', function() {
    $c = new ControleurAcceuil();
    $c->afficherAcceuil();
});


$app->get('/creationliste', function(){
   $c = new ControleurFomulaire();
   $c->affichageFormulaireCreationListe();
});

$app->post('/creationliste', function() use ($app){
    $c = new ControleurListe();
    $c->creationdeliste($app, $c);
});

$app->get('/finFormulaireCreationListe', function(){
    $c = new ControleurListe();
    $c->fincreationliste();
});

$app->get('/supprimerliste', function(){
    $c = new ControleurListe();
    $c->choixListeToken();
});

$app->post('/supprimerliste', function() use($app){
    $c = new ControleurListe();
    $c->suppressiondeliste($app);
});

$app->get('/listesupprimee', function(){
    $c = new ControleurListe();
    $c->listesupprimee();
});


$app->get('/liste/:token', function($token){
    $c = new ControleurListe();
    $c->tokenliste($token);
});

// Fonctionnalité 5 : Ajouter un message sur une liste
$app->get('/ajouterMessage/:token', function($token){
    $c = new ControleurListe();
    $c->afficherMessage($token);
});

$app->post('/ajouterMessage/:token', function($token) use($app){
    $c = new ControleurListe();
    $c->ajoutMessage($token,$app);
});

$app->get('/confirmationMessage', function(){
    $c = new ControleurListe();
    $c->afficherConfirmation();
});

// Fonctionnalité 1 : Afficher une liste de souhaits
$app->get('/choixListe', function(){
    $c = new ControleurListe();
    $c->choixListeToken();
});

$app->post('/choixListe', function() use($app){
    $c = new ControleurListe();
    $c->affichageChoix($app);
});

// Fonctionnalité 18 : s'authentifier
$app->get('/connexion', function(){
    $c = new ControleurUtilisateur();
    $c->affichageConnexion();
});

$app->post('/connexion', function() use($app){
    $c = new ControleurUtilisateur();
    $c->connexion($app);
});

$app->get('/deconnexion', function() use($app){
    $c = new ControleurUtilisateur();
    $c->deconnexion($app);
});

// Fonctionnalité 17 : créer un compte
$app->get('/inscription', function(){
    $c = new ControleurUtilisateur();
    $c->affichageInscription();
});

$app->post('/inscription', function() use($app){
    $c = new ControleurUtilisateur();
    $c->inscription($app);
});

// Fonctionnalité 19 : modifier son compte
$app->get('/modificationUtilisateur', function(){
    $c = new ControleurUtilisateur();
    $c->affichageModification();
});

$app->post('/modificationUtilisateur', function() use($app){
    $c = new ControleurUtilisateur();
    $c->modification($app);
});

// Fonctionnalité 3 : réserver un item et Fonctionnalité 4 : Ajouter un message avec sa réservation
$app->get('/reservation/:id', function() {
    $c = new ControleurFomulaire();
    $c->affichageFormulaireResa();
});

$app->post('/reservation/:id', function($id) use ($app){
    $c = new ControleurItem();
    $c->reserverItem($id,$app);
});

$app->get('/FinFormulaireResa', function() {
    $c = new ControleurFomulaire();
    $c->affichageFinFormulaireResa();
});

//
$app->get('/afficheritem/:token/:id', function ($token,$id){
    $c = new ControleurItem();
    $c->affichageItem($id);
});

// Fonctionnalité 8 : ajouter des items
$app->get('/AjouterItem', function(){
    $c = new ControleurListe();
    $c->choixListeToken();
});

$app->post('/AjouterItem', function() use($app){
    $c = new ControleurListe();
    $c->verificationListe($app);
});

$app->get('/AjouterItem/:token', function(){
    $c = new ControleurFomulaire();
    $c->affichageAjoutItem();
});

$app->post('/AjouterItem/:token', function($token) use($app) {
    $c = new ControleurItem();
    $c->ajouterItem($token,$app);
});

$app->get('/FinAjoutItem', function(){
    $c = new ControleurFomulaire();
    $c->affichageFinAjoutItem();
});

//
$app->get('/ajoutimage/:id', function($id){
    $c= new ControleurItem();
    $c->affichageAjoutImage($id);
});

$app->post('/ajoutimage/:id', function($id) use($app){
    $c= new ControleurItem();
    $c->ajoutImage($id,$app);
});

// Fonctionnalité 20 : Rendre une liste publique
$app->get('/rendrePublic', function(){
    $c = new ControleurListe();
    $c->choixListeToken();
});

$app->post('/rendrePublic', function() use($app){
    $c = new ControleurListe();
    $c->rendrepublic($app);
});

$app->get('/confirmationPublic', function(){
    $c = new ControleurListe();
    $c->afficherPublic();
});

// Fonctionnalité 21 : afficher les listes de souhaits publiques
$app->get('/choixPublic', function(){
    $c = new ControleurListe();
    $c->afficherChoix();
});

$app->get('/publicClassique', function(){
    $c = new ControleurListe();
    $c->afficherPublicClassique();
});

$app->get('/publicFormulaire', function(){
    $c = new ControleurListe();
    $c->afficherPublicFormulaire();
});

$app->post('/publicFormulaire', function() use($app){
    $c = new ControleurListe();
    $c->formulairePublic($app);
});

// Fonctionnalité 7 : modifier les informations générales d'une de ses listes
$app->get('/verificationToken', function(){
    $c = new ControleurListe();
    $c->choixListeToken();
});

$app->post('/verificationToken', function() use($app){
    $c = new ControleurListe();
    $c->modifierListe($app);
});

$app->get('/modificationListe/:token', function(){
    $c = new ControleurListe();
    $c->affichageModification();
});

$app->post('/modificationListe/:token', function($token) use($app){
    $c = new ControleurListe();
    $c->modification($token,$app);
});

$app->get('/confirmationListe', function(){
    $c = new ControleurListe();
    $c->affichageConfirmation();
});

// Fonctionnalité 9 : modifier un item
$app->get('/verificationID', function(){
    $c = new ControleurItem();
    $c->choixIdItem();
});

$app->post('/verificationID', function() use($app){
    $c = new ControleurItem();
    $c->verificationIDItem($app);
});

$app->get('/modificationItem/:id', function($id){
    $c = new ControleurItem();
    $c->affichageModification();
});

$app->post('/modificationItem/:id', function($id) use($app){
    $c = new ControleurItem();
    $c->modification($id,$app);
});

$app->get('/confirmationItem', function(){
    $c = new ControleurItem();
    $c->affichageConfirmation();
});

//
$app->get('/item', function(){

});


$app->get('/createur', function(){

});

$app->get('/partageListe', function(){
    $c = new ControleurListe();
    $c->choixListePartage();
});

$app->post('/partageListe', function() use($app){
    $c = new ControleurListe();
    $c->partageListe($app);
});


$app->get('/supprimerItem', function(){
    $c = new ControleurItem();
    $c->affichageSupprimerItem();
});

$app->post('/supprimerItem', function() use($app){
    $c = new ControleurItem();
    $c->suppressionItem($app);
});

$app->get('/itemSupprime', function(){
    $c = new ControleurItem();
    $c->confirmationSupprimerItem();
});

// Fonctionnalité 16 : Consulter les réservations et messages d'une de ses listes après échéance
$app->get('/verifConsultation', function(){
    $c = new ControleurListe();
    $c->choixListeToken();
});

$app->post('/verifConsultation', function() use($app){
    $c = new ControleurListe();
    $c->verificationConsultation($app);
});

$app->get('/consulterListe/:token', function($token){
    $c = new ControleurListe();
    $c->affichageConsultation($token);
});

// Fonctionnalité 15 : Consulter les réservations et messages d'une de ses listes avant échéance
$app->get('/verifConsultationAvant', function(){
    $c = new ControleurListe();
    $c->choixListeToken();
});

$app->post('/verifConsultationAvant', function() use($app){
    $c = new ControleurListe();
    $c->verificationConsultationAvant($app);
});

$app->get('/consulterListeAvant/:token', function($token){
    $c = new ControleurListe();
    $c->affichageConsultationAvant($token);
});

$app->get('/consulterListeAvantCrea/:token', function($token){
    $c = new ControleurListe();
    $c->affichageConsultationAvantCrea($token);
});

$app->get('/supprimerImage', function(){
    $c = new ControleurItem();
    $c->affichageSupprimerImage();
});

$app->post('/supprimerImage', function() use($app){
    $c = new ControleurItem();
    $c->supprimerImage($app);
});

$app->get('/confirmationImage', function(){
    $c = new ControleurItem();
    $c->affichageConfirmation();
});

$app->run();
