<?php


namespace wishlist;


use Illuminate\Database\Eloquent\Model;

class Droit extends Model {

    protected $table = "droit";
    protected $primaryKey = "id_autho";
    public $timestamps = false;

}