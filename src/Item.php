<?php
namespace wishlist;

use Illuminate\Database\Eloquent\Model;

class Item extends Model{
    protected $table = "item";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function asso() {
        return $this->belongsTo('wishlist\Liste', 'liste_id') ;
    }

    public function asso2() {
        return $this->belongsTo('wishlist\Utilisateur', 'id_uti') ;
    }
}