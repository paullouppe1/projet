<?php

namespace wishlist\controleur;
session_start();

use Slim\Slim;
use wishlist\Item;
use wishlist\Liste;
use wishlist\Utilisateur;
use wishlist\vue\VueFormulaire;
use wishlist\vue\VueListe;

class ControleurListe
{
    public function fincreationliste(){
        $v = new VueFormulaire();
        $v->render(FIN_FORMULAIRE_CREATION_LISTE);
    }

    //creation de liste
    public function creerliste($titre, $description, $date, $token){
        $liste = new Liste();
        $liste->titre = $titre;
        $liste->description = $description;
        $liste->expiration = $date;
        $liste->token = $token;
        $liste->save();
    }

    //fonction globale de creation de liste
    public function creationdeliste($app, $c){
        $token = Liste::select('token')->get();
        foreach ($token as $value){
            $out = preg_replace('~\D~', '', $value['token']);
            $out = $out+1;
            $res = "nosecure".$out;
        }

        if(isset($_POST['titre']) && isset($_POST['description']) && isset($_POST['expiration'])){
            if($_POST['titre'] != "" && $_POST['description'] != "" && $_POST['expiration'] != "") {
                $c->creerliste(filter_var($_POST['titre'],FILTER_SANITIZE_STRING), filter_var($_POST['description'], FILTER_SANITIZE_STRING), filter_var($_POST['expiration'], FILTER_SANITIZE_STRING), $res);
                $app->redirect('finFormulaireCreationListe', 301);
            }else{
                $app->redirect('creationliste', 301);
            }
        }else{
            $app->redirect('creationliste', 301);
        }
        exit();
    }

    public function supprimerListe($token){
        Liste::where('token','like', $token)->delete();
    }

    public function suppressiondeliste($app){
        $verif = $this->verificationToken($_POST['token']);
        if ($verif){
            $this->supprimerListe($_POST['token']);
            $app->redirect('listesupprimee',301);
        }else{
            $app->redirect('./supprimerliste', 301);
        }

        exit();
    }

    public function listesupprimee()
    {
        $v = new VueFormulaire();
        $v->render(LISTE_SUPPRIMEE);
    }

    public function choixListeToken(){
        $v = new VueListe(null);
        $v->render(TOKENLISTE);
    }

    public function choixListePartage(){
        $v = new VueListe(null);
        $v->render(CHOIXPARTAGE);
    }

    public function afficherListe($token){
        $liste = Liste::where('token','=',$token)->first();
        $id = $liste["no"];
        $items = Item::where('liste_id','=',$id)->get();
        $v = new VueListe($items);
        $v->render(LISTE_DETAIL);
    }

    public function tokenliste($token){
        $this->afficherListe($token);
        if(!(isset($_COOKIE['Fonction1']))){
            setcookie("Fonction1", 1, time()+3600);
        }
    }

    public function verificationToken($token){
        $liste = Liste::where('token','=',$token)->first();
        if(empty($liste)){
            return false;
        }
        return true;
    }

    public function afficherPublic(){
        $v = new VueListe(null);
        $v->render(CONFPUBLIC);
    }

    public function afficherChoix(){
        $v = new VueListe(null);
        $v->render(CHOIX_PUBLIC);
    }

    public function afficherPublicClassique(){
        $listes = Liste::where([['public', '=', true]])->orderBy('expiration', 'ASC')->get();
        $v = new VueListe($listes);
        $v->render(LISTE_PUBLIC);
    }

    public function afficherPublicFormulaire(){
        $v = new VueListe(null);
        $v->render(FORMULAIRE_PUBLIC);
    }

    public function affichageModification(){
        $v = new VueListe(null);
        $v->render(MODIF);
    }

    public function affichageConfirmation(){
        $v = new VueListe(null);
        $v->render(CONFIMATION);
    }

    public function afficherMessage($token){
        $liste = Liste::where('token','=',$token)->first();
        $v = new VueListe($liste);
        $v->render(ADDMESSAGE);
    }

    public function ajoutMessage($token,Slim $app){
        //Selection de la liste
        $liste = Liste::where('token','=',$token)->first();
        //modification de la bdd
        $liste->message = $_POST['message'];
        $liste->save();
        //Redirection
        $app->redirect('../confirmationMessage', 301);
        exit();
    }

    public function afficherConfirmation(){
        $v = new VueListe(null);
        $v->render(CONFIMATION_MESSAGE);
    }

    public function affichageConsultation($token){
        $liste = Liste::where('token','=',$token)->first();
        $v = new VueListe($liste);
        $v->render(CONSULTATION);
    }

    public function affichageChoix(Slim $app){
        //Vérification de la validité du token
        $verif = $this->verificationToken($_POST['token']);
        if($verif){
            $token = $_POST['token'];
            //Redirection
            $app->redirect("./liste/$token",301);
        }else{
            //Redirection
            $app->redirect("./choixListe",301);
        }
        exit();
    }

    public function verificationListe(Slim $app){
        //Vérification de la validité du token
        $verif = $this->verificationToken($_POST['token']);
        if($verif){
            $token = $_POST['token'];
            //Redirection
            $app->redirect("./AjouterItem/$token",301);
        }else{
            //Redirection
            $app->redirect("./choixListe",301);
        }
        exit();
    }

    public function rendrepublic(Slim $app){
        //Vérification de la validité du token
        $verif = $this->verificationToken($_POST['token']);
        if($verif){
            //Selection de la liste
            $liste = Liste::where('token','=',$_POST['token'])->first();
            //Modification de la bdd
            $liste->public = true;
            $liste->save();
            //Redirection
            $app->redirect("./confirmationPublic",301);
        }else{
            //Redirection
            $app->redirect("./rendrePublic",301);
        }
        exit();
    }

    public function formulairePublic(Slim $app){
        if(!empty($_POST['nom']) && !empty($_POST['debut']) && !empty($_POST['fin'])){
            //Selection de l'utilisateur
            $user = Utilisateur::where('nom',"=",$_POST['nom'])->first();
            //Selection des listes
            $listes = Liste::where([['public', '=', true],
                ['user_id', '=', $user->id_uti],
                ['expiration','>=',$_POST['debut']],
                ['expiration','<=',$_POST['fin']]])->get();
            $v = new VueListe($listes);
            $v->render(LISTE_PUBLIC);
        }else{
            //Redirection
            $app->redirect("./publicFormulaire",301);
        }
        exit();
    }

    public function modifierListe(Slim $app){
        //Vérification de la validité du token
        $verif = $this->verificationToken($_POST['token']);
        if($verif){
            $token = $_POST['token'];
            //Redirection
            $app->redirect("./modificationListe/$token",301);
        }else{
            //Redirection
            $app->redirect("./verificationToken",301);
        }
        exit();
    }

    public function modification($token,Slim $app){
        //Selection de la liste
        $liste = Liste::where('token','=',$token)->first();
        if(!empty($_POST['id_uti'])){
            //modification de la bdd
            $liste->user_id = $_POST['id_uti'];
        }
        if(!empty($_POST['titre'])){
            //modification de la bdd
            $liste->titre = $_POST['titre'];
        }
        if(!empty($_POST['description'])){
            //modification de la bdd
            $liste->description = $_POST['description'];
        }
        if(!empty($_POST['expiration'])){
            //modification de la bdd
            $liste->expiration = $_POST['expiration'];
        }
        $liste->save();
        //Redirection
        $app->redirect("../confirmationListe",301);
        exit();
    }

    public function verificationConsultation(Slim $app){
        //Vérification de la validité du token
        $verif = $this->verificationToken($_POST['token']);
        //Selection de la liste
        $liste = Liste::where('token','=',$_POST['token'])->first();
        if(isset($_SESSION['id_uti'])) {
            if ($verif && $liste->expiration < date('Y/m/d') && $_SESSION['id_uti'] == $liste->user_id) {
                $token = $_POST['token'];
                //Redirection
                $app->redirect("./consulterListe/$token", 301);
            } else {
                //Redirection
                $app->redirect("./verifConsultation", 301);
            }
        }else{
            //Redirection
            $app->redirect("./verifConsultation", 301);
        }
        exit();
    }

    public function verificationConsultationAvant(Slim $app){
        //Vérification de la validité du token
        $verif = $this->verificationToken($_POST['token']);
        //Selection de la liste
        $liste = Liste::where('token','=',$_POST['token'])->first();
        if(isset($_SESSION['id_uti'])) {
            if ($verif && $liste->expiration > date('Y/m/d') && $_SESSION['id_uti'] != $liste->user_id) {
                $token = $_POST['token'];
                //Redirection
                $app->redirect("./consulterListeAvant/$token", 301);
            } else {
                if ($verif && $liste->expiration > date('Y/m/d')) {
                    $token = $_POST['token'];
                    //Redirection
                    $app->redirect("./consulterListeAvantCrea/$token", 301);
                } else {
                    //Redirection
                    $app->redirect("./verifConsultationAvant", 301);
                }
            }
        }
        else{
            //Redirection
            $app->redirect("./verifConsultationAvant", 301);
        }
        exit();
    }

    public function affichageConsultationAvant($token){
        $liste = Liste::where('token','=',$token)->first();
        $v = new VueListe($liste);
        $v->render(CONSULTATIONAVANT);
    }

    public function affichageConsultationAvantCrea($token){
        $liste = Liste::where('token','=',$token)->first();
        $v = new VueListe($liste);
        $v->render(CONSULTATIONAVANTCREA);
    }


    public function partageListe(Slim $app){
        $verif = $this->verificationToken($_POST['token']);
        if($verif){
            $token = $_POST['token'];
            $url='http://localhost/TD/projet/liste/'.$token;
            $v = new \wishlist\vue\VueListe($url);
            $v->render(PARTAGE);
        }else{
            $app->redirect("./partageListe",301);
        }
        exit();
    }
}