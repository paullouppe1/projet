<?php


namespace wishlist\controleur;

use wishlist\vue\VueFormulaire;

class ControleurFomulaire
{
    public function affichageFormulaireResa(){
        $x = new VueFormulaire();
        $x->render(FORM_RESA);

    }

    public function affichageFinFormulaireResa(){
        $x = new VueFormulaire();
        $x->render(FIN_FORMULAIRE);
    }

    public function affichageFormulaireCreationListe(){
        $v = new VueFormulaire();
        $v->render(FORM_CREATION_LISTE);
    }

    public function afficherAcceuil(){
        $x = new VueFormulaire();
        $x->render(-1);
    }


    public function affichageAjoutItem(){
        $v = new VueFormulaire();
        $v->render(ADDITEM);
    }

    public function affichageFinAjoutItem(){
        $v = new VueFormulaire();
        $v->render(FINADDITEM);
    }
}