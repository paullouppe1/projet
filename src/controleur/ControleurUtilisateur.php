<?php

namespace wishlist\controleur;

use Slim\Slim;
use wishlist\Authentification;
use wishlist\Utilisateur;
use wishlist\vue\VueFormulaire;
use wishlist\vue\VueUtilisateur;


class ControleurUtilisateur{

    public function affichageInscription(){
        $v = new VueUtilisateur();
        $v->render(INSCRIPTION);
    }

    public function affichageConnexion(){
        $v = new VueUtilisateur();
        $v->render(CONNEXION);
    }

    public function affichageModification(){
        $v = new VueUtilisateur();
        $v->render(MODIFICATION);
    }

    public function connexion(Slim $app){
        if(!empty($_POST['login']) && !empty($_POST['mdp'])){
            // Vérification du login et du mdp
            if(Authentification::authenticate($_POST['login'],$_POST['mdp'])){
                //Redirection
                $app->redirect(".",301);
            }else{
                //Redirection
                $app->redirect("./connexion",301);
            }
        }else{
            //Redirection
            $app->redirect("./connexion",301);
        }
        exit();
    }

    public function deconnexion(Slim $app){
        // Vérification de la session
        if(isset($_SESSION['enCours'])){
            unset($_SESSION['enCours']);
        }

        //Redirection
        $app->redirect(".",301);
        exit();
    }

    public function inscription(Slim $app){
        if(!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['login']) && !empty($_POST['mdp'])){
            // Vérification de la création du compte
            if(Authentification::createUser($_POST['nom'],$_POST['prenom'],$_POST['login'],$_POST['mdp'])){
                //Redirection
                $app->redirect(".",301);
            }
            //Redirection
            $app->redirect("./inscription",301);
        }else{
            //Redirection
            $app->redirect("./inscription",301);
        }
        exit();
    }

    public function modification(Slim $app){
        //Selection de l'utilisateur
        $utilisateur = Utilisateur::where('id_uti',"=",$_SESSION['id_uti'])->first();
        if(!empty($_POST['nom'])){
            //modification de la bdd
            $utilisateur->nom = $_POST['nom'];
        }
        if(!empty($_POST['prenom'])){
            //modification de la bdd
            $utilisateur->prenom = $_POST['prenom'];
        }
        if(!empty($_POST['mdp'])){
            //modification de la bdd
            $utilisateur->mdp = $_POST['mdp'];
            //Redirection
            $app->redirect("connexion",301);
        }
        $utilisateur->save();
        //Redirection
        $app->redirect("./",301);
        exit();
    }
}