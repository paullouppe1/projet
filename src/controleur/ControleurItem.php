<?php


namespace wishlist\controleur;


use Slim\Slim;
use wishlist\Item;
use wishlist\Liste;
use wishlist\Utilisateur;
use wishlist\vue\VueFormulaire;
use wishlist\vue\VueItem;

class ControleurItem{

    public function affichageItem($id){
        $item = Item::where('id','=',$id)->first();
        $v = new VueItem($item);
        $v->render(AFFICHAGEITEM);
    }

    public function affichageModification(){
        $v = new VueItem(null);
        $v->render(MODIF);
    }

    public function verificationResa($id){
        if(!empty($item = Item::where('id','=',$id)->first())){
            if($item->reservation == 0){
                return false;
            }else{
                return true;
            }
        }
        return null;
    }

    public function modification($id, Slim $app){
        if(!empty($item = Item::where('id','=',$id)->first())) {
            if ($item->reservation == 0) {
                if (!empty($_POST['nom'])) {
                    //modification de la bdd
                    $item->nom = $_POST['nom'];
                }
                if (!empty($_POST['description'])) {
                    //modification de la bdd
                    $item->descr = $_POST['description'];
                }
                if (!empty($_POST['image'])) {
                    //modification de la bdd
                    $item->img = $_POST['image'];
                }
                if (!empty($_POST['prix'])) {
                    //modification de la bdd
                    $item->tarif = $_POST['prix'];
                }
                $item->save();
                //Redirection
                $app->redirect("../confirmationItem", 301);
            } else {
                //Redirection
                $app->redirect("../", 301);
            }
            exit();
        }
    }

    public function affichageConfirmation(){
        $v = new VueItem(null);
        $v->render(CONFIRMATION);
    }


    public function choixIdItem(){
        $v = new VueItem(null);
        $v->render(IDITEM);
    }

    public function verificationIDItem($app){
        //Selection de l'item
        $item = Item::where('id','=',$_POST['id'])->first();
        if(!empty($item)){
            $id = $_POST['id'];
            //Redirection
            $app->redirect("./modificationItem/$id",301);
        }else{
            //Redirection
            $app->redirect("./verificationID",301);
        }
        exit();
    }

    public function affichageSupprimerItem()
    {
        $v = new VueItem(null);
        $v->render(IDITEM);
    }

    public function affichageAjoutImage($id){
        $item = Item::where('id','=',$id)->first();
        $v = new VueItem($item);
        $v->render(AJOUTIMAGE);
    }

    public function ajoutImage($id,$app)
    {
        if(!empty($_FILES)){
            $target_dir = "./img";
            $target_file = $target_dir . '/' . basename($_FILES["img"]["name"]);
            move_uploaded_file($_FILES["img"]["tmp_name"], $target_file);
            $item = Item::where('id','=',$id)->first();
            $item->img=$_FILES["img"]["name"];
            $item->save();
        }
        else{
            $item = Item::where('id','=',$id)->first();
            $item->img=$_POST["url"];
            $item->save();
        }
        $liste_id = $item->liste_id;
        $liste = Liste::where('no','=',$liste_id)->first();
        $token=$liste->token;
        $app->redirect("../afficheritem/$token/$id",301);
    }

    public function confirmationSupprimerItem()
    {
        $v = new VueItem(null);
        $v->render(ITEM_SUPP);
    }

    public function supprimerItem($id){
        Item::where('id','like', $id)->delete();
    }

    public function reserverItem($id, Slim $app){
        if(!(isset($_COOKIE['Fonction3'])) && !empty($_POST['nom'])){
            setcookie("Fonction3", $_POST['nom'], time()+3600);
        }

        //Selection de l'utilisateur
        $user = Utilisateur::where([['login', '=', $_POST['login']],
            ['nom','=',$_POST['nom']],
            ['prenom','=',$_POST['prenom']]])->first();

        if(!empty($user)) {
            if (!empty($item = Item::where('id','=', $id)->first())) {
                if ($item->reservation == 0) {
                    //Modification de la bdd
                    $item->message = $_POST['message'];
                    $item->id_uti = $user->id_uti;
                    $item->reservation = true;
                    $item->save();
                    //Redirection
                    $app->redirect("../FinFormulaireResa", 301);
                }
            }
        }
        //Redirection
        $app->redirect("../reservation/$id",301);
        exit();
    }

    public function ajouterItem($token,Slim $app){
        if(!empty($_POST['nom'])){
            if(empty($_POST['prix'])){
                $_POST['prix'] = 0;
            }
            $liste = Liste::where('token','=',$token)->first();

            //ajout de l'item dans la bdd
            $item = new Item();
            $item->liste_id = $liste['no'];
            $item->nom = $_POST['nom'];
            $item->descr = $_POST['description'];
            $item->tarif = $_POST['prix'];
            if(!empty($_POST['lien'])) {
                $lien = $_POST['lien'];
                $item->descr = $item->descr . <<<End
            <a href="$lien"> lien </a><br/>
End;
            }
            $item->save();
            //Redirection
            $app->redirect("../FinAjoutItem",301);
        }else{
            //Redirection
            $app->redirect("../AjouterItem/$token",301);
        }
        exit();
    }

    public function suppressionItem($app)
    {
        $noitem = $_POST['id'];
        $item = \wishlist\Item::where('id','like',$noitem)->get();
        $item = substr($item, -3);
        $reserve = substr($item, 0, -2);

        if ($reserve == 0){
            $this->supprimerItem($noitem);
            $app->redirect("./itemSupprime",301);
        }else{
            $app->redirect("./supprimerItem",301);
        }

        exit();
    }

    public function affichageSupprimerImage(){
        $v = new VueItem(null);
        $v->render(SUPPRIMAGE);
    }

    public function supprimerImage($app)
    {
        $noitem = $_POST['id'];
        $item = \wishlist\Item::where('id','like',$noitem)->first();
        $item->img = null;
        $item->save();
        $app->redirect("./confirmationImage", 301);

        exit();
    }
}