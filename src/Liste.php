<?php
namespace wishlist;

use Illuminate\Database\Eloquent\Model;

class Liste extends Model{
    protected $table = "liste";
    protected $primaryKey = "no";
    public $timestamps = false;

    public function asso() {
        return $this->hasMany('wishlist\Item', 'liste_id', 'no') ;
    }
}