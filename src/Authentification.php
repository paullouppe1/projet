<?php

namespace wishlist;

class Authentification {

    public static function createUser($userName, $userSurmane, $userLogin, $userPwd){
        if(strlen($userPwd) >= 4 && empty(Utilisateur::where('login',"=",$userLogin)->first())){
            $tmp = new Utilisateur();
            $tmp->nom = $userName;
            $tmp->prenom = $userSurmane;
            $tmp->login = $userLogin;
            $tmp->mdp = $userPwd;
            $tmp->save();
            return true;
        }
        return false;
    }


    public static function authenticate($userLogin, $userPwd){
        $tmp = Utilisateur::where([['login',"=",$userLogin],['mdp',"=",$userPwd]])->first();
        if(!empty($tmp)){
            Authentification::loadProfile($tmp->id_uti);
            return true;
        }
        return false;
    }

    private static function loadProfile( $userID) {
        $tmp = Utilisateur::where('id_uti',"=",$userID)->first();
        $auto = $tmp->asso;
        if(isset($_SESSION['enCours'])){
            session_destroy();
        }
        session_start();
        $_SESSION['enCours'] = true;
        $_SESSION['niveau'] = $auto->niveau;
        $_SESSION['id_uti'] = $tmp->id_uti;
        $_SESSION['nom_uti'] = $tmp->nom;
    }

    public static function checkAccessRights($required){
        if($_SESSION['niveau'] < $required){
            return true;
        }
        return false;
    }
}