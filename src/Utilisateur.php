<?php

namespace wishlist;

use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model {

    protected $table = "utilisateur";
    protected $primaryKey = "id_uti";
    public $timestamps = false;

    public function asso() {
        return $this->belongsTo('wishlist\Droit', 'id_droit') ;
    }
}