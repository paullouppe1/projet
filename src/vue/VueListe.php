<?php


namespace wishlist\vue;

define('RESULTAT_CREATION',1);
define('TOKENLISTE',2);
define('LISTE_DETAIL',3);
define('CONFPUBLIC',4);
define('CHOIX_PUBLIC',5);
define('LISTE_PUBLIC',6);
define('FORMULAIRE_PUBLIC',7);
define('MODIF',8);
define('CONFIMATION',9);
define('PARTAGE',10);
define('CHOIXPARTAGE',11);
define('ADDMESSAGE',12);
define('CONFIMATION_MESSAGE',13);
define('CONSULTATION',14);
define('CONSULTATIONAVANT',15);
define('CONSULTATIONAVANTCREA',16);

class VueListe extends Vue{

    private $var;

    public function __construct($v){
        $this->var = $v;
    }

    public function render($selecteur){
        $title = $this->renderTitle();
        $nav = $this->renderMenu();
        $footer = $this->renderFooter();
        $css  = <<< end
        <link href="css/style.css" rel="stylesheet">
end;

        switch ($selecteur){
            case RESULTAT_CREATION :
                $content = '';
                break;
            case TOKENLISTE :
                $content = $this->afficherChoixListeToken();
                break;
            case LISTE_DETAIL :
                $css  = <<< end
        <link href="../css/style.css" rel="stylesheet">
end;
                $content = $this->afficherListeD();
                break;
            case CONFPUBLIC:
                $content = $this->afficherConfirmationPublic();
                break;
            case CHOIX_PUBLIC:
                $content = $this->afficherChoixPublic();
                break;
            case LISTE_PUBLIC:
                $content = $this->afficherPublicClassique();
                break;
            case FORMULAIRE_PUBLIC:
                $content = $this->afficherFormulairePublic();
                break;
            case MODIF:
                $css  = <<< end
        <link href="../css/style.css" rel="stylesheet">
end;
                $content = $this->affichageModif();
                break;
            case CONFIMATION:
                $content = $this->afficherConfirmationListe();
                break;
            case PARTAGE:
                $content = $this->afficherPartageListe();
                break;
            case CHOIXPARTAGE:
                $content = $this->afficherChoixListePartage();
                break;
            case ADDMESSAGE:
                $css  = <<< end
        <link href="../css/style.css" rel="stylesheet">
end;
                $content = $this->afficherAjoutMessage();
                break;
            case CONFIMATION_MESSAGE:
                $content = $this->afficherConfirmationMessage();
                break;
            case CONSULTATION:
                $css  = <<< end
        <link href="../css/style.css" rel="stylesheet">
end;
                $content = $this->affichageConsultation();
                break;
            case CONSULTATIONAVANT:
                $css  = <<< end
        <link href="../css/style.css" rel="stylesheet">
end;
                $content = $this->affichageConsultationAvant();
                break;
            case CONSULTATIONAVANTCREA:
                $css  = <<< end
        <link href="../css/style.css" rel="stylesheet">
end;
                $content = $this->affichageConsultationAvantCrea();
                break;
        }

        $html = <<<END
        <!DOCTYPE html>
        <html lang="en">
            <head>
            <meta charset="UTF-8">
            <title>Titre</title>
            $css
            </head>
            <body>
                $title
                $nav
                $content
            </body>
            $footer
        </html>
END;
        echo $html;

    }

    private function afficherChoixListeToken(){
        return <<<End
        <div id="formulaire">
        <h2> Choix de liste : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignement </legend>
            <p>
                <label for="token">Token :</label>
                <input type="text" name="token" />
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function afficherChoixListePartage(){
        return <<<End
        <div id="formulaire">
        <h2> Choix de la liste à partager : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignement </legend>
            <p>
                <label for="token">Token :</label>
                <input type="text" name="token" />
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function afficherListeD(){
        $res = '';
        $liste = $this->var[0]->asso;
        $titre = $liste['titre'];
        $description = $liste['description'];
        $message = $liste['message'];
        $token = $liste['token'];
        $res = $res . <<<End
        <nav class="content">
        <div class="text1"> <strong>Titre de la liste :</strong> $titre <br/> 
        <strong>Description :</strong> $description <br/> 
        <strong>Message :</strong> $message <br/> 
        <a href="../ajouterMessage/$token" class="text1">Ajouter un message</a><br/>
        <strong>Contenu :</strong> <br/>
        </div>
End;
        foreach ($this->var as $key => $value){
            $x = $value["nom"];
            $img = $value["img"];
            $id = $value['id'];
            $token = $value->asso->token;
            //Verification et affichage de l'état de la réservation
            $resa = "";
            if((isset($_COOKIE['Fonction1']) && $_COOKIE['Fonction1'] == $value->asso->user_id) || date('Y-m-d')>$value->asso->expiration){
                if($value["reservation"] == 0){
                    $resa = "Etat de la réservation : non réservé";
                }else{
                    $resa = "Etat de la réservation : réservé";;
                }
            }

            if(file_exists("img/$img") && $img != null){
                $res = $res . <<<END
            <div id="formulaire">
            <fieldset>
            <legend> $x </legend>
                <p>
                    <a href="../afficheritem/$token/$id"><img src="../img/$img" alt="$img"></a><br>
                    $resa
                </p>
            </fieldset>
            </div>
END;
            }
            else{
                $res = $res . <<<END
            <div id="formulaire">
            <fieldset>
            <legend> $x </legend>
                <p>
                    <a href="../afficheritem/$token/$id"><h4>Consulter la page de l'item</h4></a>
                    $resa
                </p>
            </fieldset>
            </div>
END;
            }
        }
        return $res . "</nav>";
    }

    private function afficherConfirmationPublic(){
        return <<<End
         <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre liste a bien été rendu public ! 
            </p>
        </fieldset>
        </div>
End;
    }

    private function afficherChoixPublic(){
        return <<<End
        <div id="formulaire">
         <h2>Affichage des listes publics</h2>
        <fieldset>
            <p>
                - <a href="./publicClassique" class="text1"> Liste Public classique</a>
            </p>
            <p>
                - <a href="./publicFormulaire" class="text1"> Liste Public avec Recherche</a>
            </p>
        </fieldset>
        </div>
End;
    }

    private function afficherPublicClassique(){
        $res = <<<End
        <div id="formulaire">
         <h2>Listes publiques</h2>
        <fieldset>
End;
        foreach ($this->var as $key => $value) {
            $x = $value->titre;
            $token = $value->token;
            $res = $res . <<<End
            <p>
                - <a href="./liste/$token"" class="text1"> $x</a>
            </p>
End;
        }
        return $res . <<<End
        </fieldset>
        </div>
End;
    }

    private function afficherFormulairePublic(){
        return <<<End
        <div id="formulaire">
        <h2> Recherche de liste publique : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="nom">Nom de l'auteur :</label>
                <input type="text" name="nom">
            </p>
            <p>
                <label for="debut">A partir de  :</label>
                <input type="date" name="debut">
            </p>
            <p>
                <label for="fin">Jusqu'à :</label>
                <input type="date" name="fin">
            </p>
            <p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function affichageModif(){
        return <<<End
        <div id="formulaire">
        <h2> Modification : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="id_uti">Id de l'utilisateur :</label>
                <input type="text" name="id_uti">
            </p>
            <p>
                <label for="titre">Titre :</label>
                <input type="text" name="titre">
            </p>
            <p>
                <label for="description">Description :</label>
                <textarea name="description" rows="8" cols="45"></textarea>
            </p>
            <p>
                <label for="expiration">Expiration :</label>
                <input type="date" name="expiration">
            </p>
            <input type="submit" value="Valider">
            <input type="reset" value="Annuler">
        </fieldset>
        </form>
        </div>
End;
    }

    private function afficherConfirmationListe(){
        return <<<End
         <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre liste a bien été mise à jour ! 
            </p>
        </fieldset>
        </div>
End;
    }

    private function afficherPartageListe()
    {
        return <<<End
         <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Lien de partage </legend>
            <p>
                $this->var
            </p>
        </fieldset>
        </div>
End;
    }

    private function afficherAjoutMessage(){
        return <<<End
        <div id="formulaire">
        <h2> Ajout d'un message : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="message">Description :</label>
                <textarea name="message" rows="8" cols="45"></textarea>
            </p>
            <input type="submit" value="Valider">
            <input type="reset" value="Annuler">
        </fieldset>
        </form>
        </div>
End;
    }

    private function afficherConfirmationMessage(){
        return <<<End
         <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre message a bien été enregistré !
            </p>
        </fieldset>
        </div>
End;
    }

    private function affichageConsultation(){
                $res = '';


                        $titre = $this->var->titre;
                        $description = $this->var->description;
                        $message = $this->var->message;
                        $res = $res . <<<End
           <nav class="content">
            <div class="text1"> <strong>Titre de la liste :</strong> $titre <br/> 
            <strong>Description :</strong> $description <br/> 
            <strong>Message :</strong> $message <br/> 
            <strong>Contenu :</strong> <br/>
            </div>
End;

            foreach ($this->var->asso as $key => $value){
                if($value["reservation"] == 1){
                    $x = $value["nom"];
                    $img = $value["img"];

                    $uti =  $value->asso2;
                    $utiNom = $uti['nom'];
                    $utiPrenom = $uti['prenom'];
                    $resa = "Item réservé par : " . $utiNom . ' ' . $utiPrenom;

                    $res = $res . <<<END
                    <div id="formulaire">
                    <fieldset>
                    <legend> $x </legend>
                        <p>
                            <img src="../img/$img" alt="$img"><br>
                            $resa
                        </p>
                        <p>
                            Message : $value->message
                        </p>
                    </fieldset>
                    </div>
END;
                }
            }
            $res = $res . '</nav>';



        return $res;
    }

    private function affichageConsultationAvant()
    {
        $res = '';


            $titre = $this->var->titre;
            $description = $this->var->description;
            $res = $res . <<<End
           <nav class="content">
            <div class="text1"> <strong>Titre de la liste :</strong> $titre <br/> 
            <strong>Description :</strong> $description <br/> 
            <strong>Contenu :</strong> <br/>
            </div>
End;

            foreach ($this->var->asso as $key => $value){
                if($value["reservation"] == 1){
                    $x = $value["nom"];
                    $img = $value["img"];

                    $uti =  $value->asso2;
                    $utiNom = $uti['nom'];
                    $utiPrenom = $uti['prenom'];
                    $resa = "Item réservé par : " . $utiNom . ' ' . $utiPrenom;

                    $res = $res . <<<END
                    <div id="formulaire">
                    <fieldset>
                    <legend> $x </legend>
                        <p>
                            <img src="../img/$img" alt="$img"><br>
                            $resa
                        </p>
                        <p>
                        </p>
                    </fieldset>
                    </div>
END;
                }
            }
            $res = $res . '</nav>';



        return $res;
    }

    private function affichageConsultationAvantCrea()
    {
        $res = '';


            $titre = $this->var->titre;
            $description = $this->var->description;
            $res = $res . <<<End
           <nav class="content">
            <div class="text1"> <strong>Titre de la liste :</strong> $titre <br/> 
            <strong>Description :</strong> $description <br/> 
            <strong>Contenu :</strong> <br/>
            </div>
End;

            foreach ($this->var->asso as $key => $value){
                if($value["reservation"] == 1){
                    $x = $value["nom"];
                    $img = $value["img"];


                    $res = $res . <<<END
                    <div id="formulaire">
                    <fieldset>
                    <legend> $x </legend>
                        <p>
                            <img src="../img/$img" alt="$img"><br>
                        </p>
                        <p>
                        </p>
                    </fieldset>
                    </div>
END;
                }
            }
            $res = $res . '</nav>';



        return $res;
    }
}