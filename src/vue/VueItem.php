<?php


namespace wishlist\vue;

define('MODIF',1);
define('CONFIRMATION',2);
define('IDITEM',3);
define('ITEM_SUPP',4);
define('AFFICHAGEITEM',5);
define('AJOUTIMAGE',6);
define('SUPPRIMAGE',7);


class VueItem extends Vue{

    private $var;

    public function __construct($v){
    $this->var = $v;
    }

    public function render($selecteur){
    $title = $this->renderTitle();
    $nav = $this->renderMenu();
    $footer = $this->renderFooter();
    $css  = <<< end
        <link href="css/style.css" rel="stylesheet">
end;

    switch ($selecteur){
        case MODIF:
            $css  = <<< end
        <link href="../css/style.css" rel="stylesheet">
end;
            $content = $this->affichageModif();
            break;
        case CONFIRMATION:
            $content = $this->affichageConfirmationItem();
            break;
        case IDITEM:
            $content = $this->choixId();
            break;

        case ITEM_SUPP:
            $content = $this->itemSupp();
            break;

        case AFFICHAGEITEM:
            $content= $this->affichageItem();
            $css  = <<< end
        <link href="../../css/style.css" rel="stylesheet">
end;

            break;

        case AJOUTIMAGE:
            $content=$this->ajoutImage();
            $css  = <<< end
        <link href="../css/style.css" rel="stylesheet">
end;
            break;
        case SUPPRIMAGE:
            $content=$this->supprimerImage();
            break;
    }

    $html = <<<END
        <!DOCTYPE html>
        <html lang="en">
            <head>
            <meta charset="UTF-8">
            <title>Titre</title>
            $css
            </head>
            <body>
                $title
                $nav
                $content
            </body>
            $footer
        </html>
END;
    echo $html;

}

    private function affichageItem(){
        $tmp = $this->var;
        $tmp2=$tmp->asso;
        if($tmp2->reservation==1){
            $reserv = "Résevé !";
        }
        else{
        $reserv = <<<end
            Pas réservé ! :
            <a href="../../reservation/$tmp->id" class="text1"> Réservation </a>
end;
        }
        if(file_exists("img/$tmp->img") && $tmp->img!=null ){
            $img = <<<end
<img src="../../img/$tmp->img">
end;

        }
        else{
                $img = <<<end
            Pas d'image ! : 
            <a href="../../ajoutimage/$tmp->id" class="text1"> Ajouter une image </a>
end;
        }
        return <<<End
        <div id="formulaire">
        <h2> $tmp->nom : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignement </legend>
            <p class="text1"> Appartient à la liste $tmp2->titre </p>
            $img
            <p class="text1"> Description : $tmp->descr</p>
            <p class="text1"> Prix : $tmp->tarif €</p>
            <p class="text1">$reserv</p>
        </fieldset>
        </form>
        </div>
End;

    }

    private function ajoutImage(){
        $tmp = $this->var;
        $tmp2=$tmp->asso;
        return <<<End
        <div id="formulaire">
        <h2> $tmp->nom : </h2>
        <form method="post" class="content" id="menu" enctype="multipart/form-data">
        <fieldset>
        <legend> Ajout d'une image </legend>
            <p class="text1"> Appartient à la liste $tmp2->titre </p>
                <label for="url">URL ou chemin relatif : </label>
                <input type="text" name="url" />
            <input type = "file" name="img" id="img" accept=".png, .jpg,.jpeg">
            <input type="submit" value="Envoyer">
        </fieldset>
        </form>
        </div>
End;

    }

    private function affichageModif(){
        return <<<End
        <div id="formulaire">
        <h2> Modification Item : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="nom">Nom :</label>
                <input type="text" name="nom">
            </p>
            <p>
                <label for="description">Description :</label>
                <textarea name="description" rows="8" cols="45"> </textarea>
            </p>
            <p>
                <label for="image">Image :</label>
                <input type="text" name="image">
            </p>
            <p>
                <label for="prix">Prix :</label>
                <input type="number" step="0.01" name="prix" min="0.01" max="99999">
            </p>
            <input type="submit" value="Valider">
            <input type="reset" value="Annuler">
        </fieldset>
        </form>
        </div>
End;
    }

    private function affichageConfirmationItem(){
        return <<<End
         <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre item a bien été mise à jour ! 
            </p>
        </fieldset>
        </div>
End;
    }

    private function choixId(){
        return <<<End
        <div id="formulaire">
        <h2> Choix de l'item : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignement </legend>
            <p>
                <label for="id">Id :</label>
                <input type="text" name="id" />
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function itemSupp()
    {
        return <<<End
         <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre item a bien été supprimé ! 
            </p>
        </fieldset>
        </div>
End;
    }

    private function supprimerImage()
    {
        return <<<End
        <div id="formulaire">
        <h2> Choix de l'item : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignement </legend>
            <p>
                <label for="id">Id :</label>
                <input type="text" name="id" />
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

}