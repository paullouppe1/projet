<?php


namespace wishlist\vue;


class VueAcceuil extends Vue
{

    public function render($selecteur)
    {
        $title = $this->renderTitle();
        $menu = $this->renderMenu();
        $footer = $this->renderFooter();
        $content = $this->renderContent();


        $html = <<<END
        <!DOCTYPE html>
        <html lang="en">
            <head>
            <meta charset="UTF-8">
            <title>Titre</title>
            <link href="css/style.css" rel="stylesheet">
            
        </head>
            <body>
            $title
            $menu
            $content
            </body>
            $footer
        </html>
END;
        echo $html;
    }

    public function renderContent(){
        $html = <<<END

END;
        return $html;
    }
}