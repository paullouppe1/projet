<?php

namespace wishlist\vue;
use wishlist;

define('FIN_FORMULAIRE_RESA',101);
define('FIN_FORMULAIRE_CREATION_LISTE',102);
define('FORM_RESA',1);
define('FORM_CREATION_LISTE',2);
define('FIN_FORMULAIRE',3);
define('ADDITEM',4);
define('FINADDITEM',5);
define('LISTE_SUPPRIMEE',6);

class VueFormulaire extends Vue
{

    public function render($selecteur){
        $title = $this->renderTitle();
        $menu = $this->renderMenu();
        $footer = $this->renderFooter();
        $css = "<link href=\"css/style.css\" rel=\"stylesheet\">";
        switch ($selecteur) {
            case FORM_RESA:
                $content = $this->FormulaireResa();
                $css = "<link href=\"../css/style.css\" rel=\"stylesheet\">";
                break;
            case FORM_CREATION_LISTE:
                $content = $this->formulaireCreationListe();
                break;
            case LISTE_SUPPRIMEE:
                $content = $this->listeSupprimee();
                break;
            case FIN_FORMULAIRE_CREATION_LISTE:
                $content = $this->finformulairecreationliste();
                break;
            case FIN_FORMULAIRE:
                $content = $this->FinFormulaire();
                break;
            case ADDITEM:
                $content = $this->AjoutItem();
                $css = "<link href=\"../css/style.css\" rel=\"stylesheet\">";
                break;
            case FINADDITEM:
                $content = $this->FinAjoutItem();
                break;
                default:
                break;
        }
        $html = <<<END
        <!DOCTYPE html>
        <html lang="en">
            <head>
            <meta charset="UTF-8">
            <title>Titre</title>
            $css
            </head>
            <body>
                $title
                $menu
                $content
            </body>
            $footer
        </html>
END;
        echo $html;
    }



    private function FormulaireResa(){
        $nom = '';
        if(isset($_COOKIE['Fonction3'])){
            $nom = htmlspecialchars($_COOKIE["Fonction3"]);
        }
        return <<<End
        <div id="formulaire">
        <h2> Réservation d'objet : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="login">Login :</label>
                <input type="text" name="login" />
            </p>
            <p>
                <label for="nom">Nom :</label>
                <input type="text" name="nom" value="$nom">
            </p>
            <p>
                <label for="prenom">Prenom :</label>
                <input type="text" name="prenom" />
            </p>
            <p>
                <label for="message">Message :</label>
                <textarea name="message" rows="8" cols="45"> </textarea>
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function FinFormulaire(){
        return <<<End
         <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre réservation a bien été prise en compte !
            </p>
        </fieldset>
        </div>
End;
    }


    private function formulaireCreationListe()
    {
        return <<<End
        <div id="formulaire">
        <h2> Création de liste : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="titre">Titre :</label>
                <input type="text" name="titre" />
            </p>
            <p>
                <label for="description">Description :</label>
                <textarea name="description" rows="8" cols="45"> </textarea>
            </p>
            <p>
                <label for="expiration">Date d'expiration :</label>
                <input type="date" name="expiration">
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function AjoutItem(){
        return <<<End
        <div id="formulaire">
        <h2> Ajout d'objet : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="nom">Nom :</label>
                <input type="text" name="nom" />
            </p>
            <p>
                <label for="description">Description :</label>
                <textarea name="description" rows="8" cols="45"> </textarea>
            </p>
            <p>
                <label for="lien">Lien :</label>
                <input type="text" name="lien" />
            </p>
            <p>
                <label for="prix">Prix :</label>
                <input type="number" step="0.01" name="prix" min="0.01" max="99999">
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function FinAjoutItem(){
        return <<<End
         <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre ajout a été prise en compte ! 
            </p>
        </fieldset>
        </div>
End;
    }

    private function finformulairecreationliste()
    {
        return <<<END
        <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre liste a bien été crée ! 
            </p>
        </fieldset>
        </div>
END;

    }

    private function listeSupprimee()
    {
        return <<<END
        <div id="formulaire">
         <h3><br></h3>
        <fieldset>
        <legend> Confirmation </legend>
            <p>
                Votre liste a bien été supprimée ! 
            </p>
        </fieldset>
        </div>
END;
    }
}