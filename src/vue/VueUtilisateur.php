<?php


namespace wishlist\vue;

define('CONNEXION', 1);
define('MODIFICATION', 2);
define('INSCRIPTION', 3);

class VueUtilisateur extends Vue
{

    public function render($selecteur)
    {
        $title = $this->renderTitle();
        $nav = $this->renderMenu();
        $footer = $this->renderFooter();
        $css = <<< end
        <link href="css/style.css" rel="stylesheet">
end;

        switch ($selecteur) {
            case CONNEXION:
                $content = $this->AffichageConnextion();
                break;
            case MODIFICATION:
                $content = $this->AffichageModification();
                break;
            case INSCRIPTION:
                $content = $this->Inscription();
                break;
            default:
                $content = 'default';
                break;
        }

        $html = <<<END
        <!DOCTYPE html>
        <html lang="en">
            <head>
            <meta charset="UTF-8">
            <title>Titre</title>
            $css
            </head>
            <body>
                $title
                $nav
                $content
            </body>
            $footer
        </html>
END;
        echo $html;
    }

    private function Inscription(){
        return <<<End
        <div id="formulaire">
        <h2> Inscription : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="nom">Nom :</label>
                <input type="text" name="nom" />
            </p>
            <p>
                <label for="prenom">Prenom :</label>
                <input type="text" name="prenom" />
            </p>
            <p>
                <label for="login">Identifiant :</label>
                <input type="text" name="login" />
            </p>
            <p>
                <label for="mdp">Mot de passe :</label>
                <input type="text" name="mdp" />
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function AffichageConnextion(){
        return <<<End
        <div id="formulaire">
        <h2> Connexion : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="login">Identifiant :</label>
                <input type="text" name="login" />
            </p>
            <p>
                <label for="mdp">Mot de passe :</label>
                <input type="text" name="mdp" />
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }

    private function AffichageModification(){
        return <<<End
        <div id="formulaire">
        <h2> Modification : </h2>
        <form method="post" class="content" id="menu">
        <fieldset>
        <legend> Renseignements </legend>
            <p>
                <label for="nom">Nom :</label>
                <input type="text" name="nom" />
            </p>
            <p>
                <label for="prenom">Prenom :</label>
                <input type="text" name="prenom" />
            </p>
            <p>
                <label for="mdp">Mot de passe :</label>
                <input type="text" name="mdp" />
            </p>
            <input type="submit" value="Valider" />
            <input type="reset" value="Annuler" />
        </fieldset>
        </form>
        </div>
End;
    }
}