<?php


namespace wishlist\vue;

abstract class Vue{

    public abstract function render($selecteur);

    public function renderTitle(){
        $res = <<<END
        <header>
        <h1>MY WISHLIST</h1>
        </header>
END;
        return $res;
    }

    public function renderMenu(){
        //if(isset($_SESSION['enCours']) || !empty($_SESSION['enCours'])) {
            $compte = <<<End
                <li><a href="http://localhost/TD/projet/connexion"> Connexion </a></li>
                <li><a href="http://localhost/TD/projet/inscription"> Inscription </a></li>
                <li><a href="http://localhost/TD/projet/modificationUtilisateur"> Modification compte </a></li>
                <li><a href="http://localhost/TD/projet/deconnexion"> Deconnexion </a></li>
End;
            $liste = <<<End
                <li><a href="http://localhost/TD/projet/verifConsultation"> Consultation Liste après échéance</a></li>
                <li><a href="http://localhost/TD/projet/verifConsultationAvant"> Consultation Liste avant échéance</a></li>
                <li><a href="http://localhost/TD/projet/creationliste"> Creation Liste </a></li>
                <li><a href="http://localhost/TD/projet/supprimerliste"> Supprimer Liste </a></li>
                <li><a href="http://localhost/TD/projet/verificationToken"> Modification Liste </a></li>
                <li><a href="http://localhost/TD/projet/choixListe"> Choix de liste </a></li>
                <li><a href="http://localhost/TD/projet/rendrePublic"> Rendre public une liste </a></li>
                <li><a href="http://localhost/TD/projet/choixPublic"> Liste public </a></li>
                <li><a href="http://localhost/TD/projet/partageListe"> partager liste </a></li>
End;
            $item = <<<End
                <li><a href=""> Item </a>
                    <ul>
                        <li><a href="http://localhost/TD/projet/AjouterItem"> Ajout d'un item </a></li>
                        <li><a href="http://localhost/TD/projet/verificationID"> Modification d'un item </a></li>
                        <li><a href="http://localhost/TD/projet/supprimerItem">Supprimer un item </a></li>
                        <li><a href="http://localhost/TD/projet/supprimerImage">Supprimer une image </a></li>
                    </ul>
                </li>
                
End;


        $res = <<<END
        <nav id="menu">
            <ul>
                <li><a href="http://localhost/TD/projet"> Acceuil </a></li>
                <li><a href=""> Liste </a>
                    <ul>
                        $liste
                    </ul>
                </li>
                $item
                <li><a href=""> Compte</a>
                    <ul>
                        $compte
                    </ul>
                </li>
            </ul>
        </nav>
END;

        return $res;
    }


    public function renderFooter(){
        $res = <<<END
        <footer>
            <p><input type="button" value="A propos"></p>
            <div class="copyright"><p>Copyright © My Wishlist 2019 – 2020 – Tous droits réservés</p></div>
        </footer>
END;
        return $res;
    }


}