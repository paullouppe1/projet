<?php

use Illuminate\Database\Capsule\Manager;
use wishlist\Item;
use wishlist\Liste;

require_once __DIR__ . '/vendor/autoload.php';
$db = new Manager();
$db->addConnection( [
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'phptd',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$nb = 3;
switch ($nb){
    case 0:
        $items = Item::get();
        foreach ($items as $item) {
            echo $item->nom . "<br>";
        }
        break;
    case 1 :
        $items = Item::select('nom')->where('id',"=",$_GET['id'])->first();
        echo $items->nom . "<br>";
        break;
    case 2:
        $item = new Item();
        $item->nom = 'Xu';
        $item->save();
        echo Item::where('nom','=','Xu')->first()->nom;
        $item->delete();
        break;
    case 3:
        $items = Item::where('liste_id',"!=",0)->get();
        foreach ($items as $item){
            $liste = $item->asso;
            echo $liste->titre . "<br>";
        }
        break;
    case 4:
        $liste = Liste::select('no')->where('id',"=",$_GET['id'])->first();
        $items = $liste->asso;
        foreach ($items as $item){
            echo $item->nom . "<br>";
        }
        break;
    default :
        $listes = Liste::get();
        foreach ($listes as $liste) {
            echo $liste->titre . "<br>";
        }
        break;
}

